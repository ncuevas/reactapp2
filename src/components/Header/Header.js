import React, { Component } from 'react'
import './Header.css';

class Header extends Component {

 render() {
   return (
   <div>
   <div className="header">
      <h1>Header</h1>
      <img src="" alt="header image"/>
      <p>subtitle</p>
    </div>
    <div className="topnav">
      <a href="#">Link</a>
      <a href="#">Link</a>
      <a href="#">Link</a>
    </div>
    </div>
   );
 }
}

export default Header;
