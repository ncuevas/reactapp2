import React, { Component } from 'react'
import './Content.css';

class Content extends Component {
 constructor(props) {
   super(props);
 }

 render() {
   return (
    <div className="column">
        <h2>{this.props.producto.name}</h2>
        <p>
          cantidad: {this.props.producto.quantity}
        </p>
        <p>
          precio: {this.props.producto.price}
        </p>
      </div>
   );
 }
}

export default Content;
