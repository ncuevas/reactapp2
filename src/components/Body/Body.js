import React, { Component } from 'react'
import './Body.css';


import Content from './Content/Content'

class Body extends Component {

 constructor(props) {
   super(props);
  }

 render() {
   return (
   <div>
    <div className="row">
      {this.props.productos.map(
            (item,index) =>
             <Content producto={item}/>
            )
        }
    </div>
   </div>
   );
 }
}

export default Body;
