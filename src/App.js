import React, { Component } from 'react'
import './App.css';

import Header from'./components/Header/Header'
import Body from'./components/Body/Body'
import Footer from'./components/Footer/Footer'


const apiUrl = 'http://localhost:8001/ecommerce/'


class App extends Component {

 constructor(props) {
   super(props);
   this.state = {
     productos: []
   };
  }

  componentDidMount () {
    console.log("ejecuta componentDidMount")
    const productosUrl = apiUrl + 'producto.php';
    fetch(productosUrl, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(data => {

        this.setState({ productos : data.response})
    });
  }

 render() {
   return (
   <div>
     <Header/>
     <Body productos={this.state.productos}/>
     <Footer/>
   </div>
   );
 }
}

export default App;
